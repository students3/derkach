﻿using EF.CodeFirst.Models;
using EF_code11.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson19
{
    class Program
    {
        static void Main(string[] args)
        {
            TestContext();
            //TestContextSecond();
            Console.WriteLine("Finished");
            Console.ReadLine();
        }

        private static void TestContext()
        {
            using (var context = new UnivercityContext())
            {
                var email = new Email { EmailValue = "test@test.test" };
                var lecturer = new Lecturer
                {
                    FirstName = "Ivan",
                    LastName = "Petrov",
                    City = "Nikolaev",
                    Phone = "55546235",
                    State = "NI",
                    Zip = "65464",
                    Address = "Frunze str. 12, apt. 56",
                    Emails = new List<Email>(new Email[] { email })

                };
                context.Lecturers.Add(lecturer);
                context.SaveChanges();
            }
        }
        private static void TestContextSecond()
        {
            using (var context = new UnivercityContext())
            {
                var email = new Email { EmailValue = "test1@test.test" };
                var email2 = new Email { EmailValue = "test2@test.test" };
                var lecturer1 = new Lecturer
                {
                    FirstName = "Olga",
                    LastName = "Petrova",
                    City = "Nikolaev",
                    Phone = "546552633",
                    State = "NI",
                    Zip = "540058",
                    Address = "Vaslyaeva str. 10, apt. 10",
                    Emails = new List<Email>(new Email[] { email })

                };
                var lecturer2 = new Lecturer
                {
                    FirstName = "Nikolai",
                    LastName = "Petrov",
                    City = "Nikolaev",
                    Phone = "5546463255",
                    State = "NI",
                    Zip = "540058",
                    Address = "Vaslyaeva str. 10, apt. 10",
                    Emails = new List<Email>(new Email[] { email2 })

                };
                context.Lecturers.Add(lecturer1);
                context.Lecturers.Add(lecturer2);
                context.SaveChanges();
            }
        }


    }
}
