﻿CREATE TABLE [dbo].[Products] (
    [ProductID]          INT           IDENTITY (1, 1) NOT NULL,
    [ProductName]        VARCHAR (50)  NOT NULL,
    [ProductPrice]       MONEY         NOT NULL,
    [ProductDescription] VARCHAR (500) NOT NULL,
    [SupplierID]         INT           NULL,
    CONSTRAINT [PK_Products_ProductID] PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [FK_Products_Suplliers_SupplierID] FOREIGN KEY ([SupplierID]) REFERENCES [dbo].[Suppliers] ([SupplierID])
);

