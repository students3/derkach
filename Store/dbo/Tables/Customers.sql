﻿CREATE TABLE [dbo].[Customers] (
    [CustomerID]      INT          IDENTITY (1, 1) NOT NULL,
    [CustomerName]    VARCHAR (15) NOT NULL,
    [CustomerPhone]   VARCHAR (15) NOT NULL,
    [CustomerEmail]   VARCHAR (15) NOT NULL,
    [CustomerDetails] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_Customers_CustomerID] PRIMARY KEY CLUSTERED ([CustomerID] ASC)
);

