﻿CREATE TABLE [dbo].[Orders] (
    [OrderID]      INT          IDENTITY (1, 1) NOT NULL,
    [CustomerID]   INT          NOT NULL,
    [OrderStatus]  BIT          NOT NULL,
    [OrderDetails] VARCHAR (20) NOT NULL,
    [OrderDate]    DATE         NOT NULL,
    CONSTRAINT [PK_Orders_OrderID] PRIMARY KEY CLUSTERED ([OrderID] ASC),
    CONSTRAINT [FK_Orders_Customers_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
);

