﻿CREATE TABLE [dbo].[OrderList] (
    [OrderID]         INT NULL,
    [ProductID]       INT NULL,
    [ProductQuantity] INT NULL,
    CONSTRAINT [FK_OrderList_Orders_OrderID] FOREIGN KEY ([OrderID]) REFERENCES [dbo].[Orders] ([OrderID]),
    CONSTRAINT [FK_OrderList_Products_ProductID] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([ProductID])
);

