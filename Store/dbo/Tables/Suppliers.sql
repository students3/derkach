﻿CREATE TABLE [dbo].[Suppliers] (
    [SupplierID]    INT          IDENTITY (1, 1) NOT NULL,
    [SupplierName]  VARCHAR (20) NOT NULL,
    [SupplierEmail] VARCHAR (15) NOT NULL,
    [SupplierPhone] VARCHAR (15) NOT NULL,
    CONSTRAINT [PK_Suplliers_SupplierID] PRIMARY KEY CLUSTERED ([SupplierID] ASC)
);

