﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson188
{
    class Program
    {
        static void Main(string[] args)
        {
            GetDataSet();
            Console.ReadLine();
        }

        private static DataTable GetHistoryDatatable()
        {
            var dtInput = new DataTable();
            dtInput.TableName = "Mapping";
            dtInput.Columns.Add("Letter", typeof(System.String));
            dtInput.Columns.Add("Column", typeof(System.String));
            dtInput.Columns.Add("MapTo", typeof(System.String));

            var dr = dtInput.NewRow();
            dr[0] = "A";
            dr[1] = "Task";
            dr[2] = "";
            dtInput.Rows.Add(dr);

            dr = dtInput.NewRow();
            dr[0] = "B";
            dr[1] = "Task";
            dr[2] = "";
            dtInput.Rows.Add(dr);

            return dtInput;
        }
        private static void GetDataSet()
        {
            var ds = new DataSet();
            ds.Tables.Add(GetHistoryDatatable());
        }

        //private static void GetData
    }
}
