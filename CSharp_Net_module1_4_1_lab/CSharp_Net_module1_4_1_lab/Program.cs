﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_4_1_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 9) declare object of OnlineShop
             OnlineShop onlineShop = new OnlineShop();
            // 10) declare several objects of Customer
            Customer customer = new Customer("Viktor");
            Customer customer1 = new Customer("Sergey");
            // 11) subscribe method GotNewGoods() of every Customer instance
            // to event NewGoodsInfo of object of OnlineShop
            onlineShop.NewGoodsInfo += customer.GotNewGoods;
            onlineShop.NewGoodsInfo += customer1.GotNewGoods;

            // 12) invoke method NewGoods() of object of OnlineShop
            // discuss results
            onlineShop.NewGoods("Bag");
            Console.ReadLine();

        }
    }
}
