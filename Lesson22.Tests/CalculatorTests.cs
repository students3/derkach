﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lesson22.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void Multiply_2by6_Expected12()
        {
            //Arrange
            var calculator = new Calculator();
            //Act
            var actual = calculator.Multiply(2,6);
            //Assert
            Assert.AreEqual(actual, 12);

        }
        [TestMethod]
        public void Sum_2and7_Expected9()
        {
            //Arrange
            var calculator = new Calculator();
            //Act
            var actual = calculator.Sum(2, 7);
            //Assert
            Assert.AreEqual(actual, 9);
        }
        [TestMethod]
        public void Del_14and2_Expected7()
        {
            //Arrange
            var calculator = new Calculator();
            //Act
            var actual = calculator.Del(14, 2);
            //Assert
            Assert.AreEqual(actual, 7);
        }
        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException), "Can't divide it by zero!")]
        public void Del_Exception()
        {
            //Arrange
            var calculator = new Calculator();
            //Act
            var actual = calculator.Del(14, 0);
            //Assert
            Assert.Fail();
        }

    }
}
