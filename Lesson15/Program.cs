﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson15
{
    class Program
    {
        static void Main(string[] args)
        {

            var rolesList = new List<Role>();

            Role Role1 = new Role { RoleId = 1, RoleName= "Admin" };
            Role Role2 = new Role { RoleId = 1, RoleName = "User" };
            Role Role3 = new Role { RoleId = 1, RoleName = "Moderator" };
            Role Role4 = new Role { RoleId = 1, RoleName = "Product Import" };

            rolesList.Add(Role1);
            rolesList.Add(Role2);
            rolesList.Add(Role3);
            rolesList.Add(Role4);

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(rolesList);

            using (var writer = new StreamWriter("MyJson.txt", false))
            {
                writer.WriteLine(json);
            }
            Console.WriteLine("Finished");
            Console.ReadLine();
        }

    }
}
