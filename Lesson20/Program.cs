﻿using Lesson20.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson20
{
    class Program
    {
        static void Main(string[] args)
        {
            TestOrderBy();
            //TestLinq1();
            Console.ReadLine();
        }
        private static void TestLinq1()
        {
            // 1. Data source
            int[] numbers = new int[5] { 0, 1, 2, 3, 4 };

            // 2. Query creation
            var numQuery = from num in numbers
                           where (num % 2) == 0
                           select num;

            // 3. Query execution.
            foreach (int num in numQuery)
            {
                Console.Write("{0, 1} ", num);
            }
            var numQuery2 = numbers.Where(x => x % 2 == 0).ToArray();

            foreach (int num in numQuery2)
            {
                Console.Write("{0, 1} ", num);
            }
            }


        private static void TestOrderBy()
        {
            var persons = new List<Person>(new[] {
               new Person {FirstName="Svetlana", LastName="Odarko", ID=1111, Age = 18},
               new Person {FirstName="Petro", LastName="Petrenko", ID=1112, Age = 25},
               new Person {FirstName="Gerasym", LastName="Gerasymenko", ID=1113, Age = 30},
               new Person {FirstName="Vasyl", LastName="Ivanenko", ID=1114, Age = 25},
               new Person {FirstName="Zakhar", LastName="Sergienko", ID=1115, Age = 20}
           });
            var query = (from itm in persons
                         orderby itm.FirstName descending, itm.Age ascending
                         select itm).ToArray();

            foreach (var item in query)
            {
                Console.WriteLine($"{item.FirstName}{item.Age}");
            }

            var query2 = persons.OrderByDescending(x => x.FirstName).ThenBy(x => x.Age).ToArray();
            foreach (var item in query2)
            {
                Console.WriteLine($"{item.FirstName}{item.Age}");
            }

        }
    }
}
