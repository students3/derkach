﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task1();
            //Task2();
            Task3();
            //Task4();
            Console.ReadLine();
        }

        static void Task1()
        {
            int j;
            for (int i = 1; i <= 150; i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
            }
            //Console.ReadLine();


            //The second way :)

            //for (int i = 2; i <= 150; i += 2)
            //{
            //        Console.WriteLine(i);
            //}
        }
        static void Task2()
        {
            int i;
            int j;

            for (i = 50; i <= 200; i++)
            {
                if (i % 2 != 0)

                    Console.WriteLine(i);
            }
            //Console.ReadLine();

        }
        static void Task3()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            int i = 0;
            int j = 200;
            int result = -1;

            do
            {
                if (arrTemp[i] == j)
                {
                    result = i;
                    Console.WriteLine("Position of number " + j + ": " + i);        
                }
                i++;
            }
            while (i < arrTemp.Length);
            //Console.ReadLine();

            if (result > -1)
            {
                Console.WriteLine("Position of number " + j + ": " + result);
            }
            else
            {
                Console.WriteLine("Number not found");
            }

        }
        static void Task4()
        {
            int[] arrTemp = { 12, 21, 54, 65, 13, 587 };
            int i;


            foreach (int element in arrTemp)
            {
                i = element * 2;
                Console.WriteLine(i);

            }
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Quantity of elements in array = " +
arrTemp.Length);
            Console.WriteLine("Does array have fixed size? - " +
arrTemp.IsFixedSize);
            Console.WriteLine("Is array read only? - " +
arrTemp.IsReadOnly);
            Console.WriteLine("Array Dimension = " + arrTemp.Rank);
            // Console.ReadLine();

        }



    }
}
