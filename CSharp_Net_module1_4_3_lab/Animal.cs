﻿using System;
using System.Collections;


namespace CSharp_Net_module1_4_3_lab
{
    // 12) change methods of sorting to properties

    // 1) implement interface IComparable
    public class Animal : IComparable
    {
        public string Genus { get; }
        public int Weight { get; }

        public Animal(string genus, int weight)
        {
            this.Genus = genus;
            this.Weight = weight;
        }

        public int CompareTo(object obj)
        {
            int result = 0;
            if (obj != null && obj is Animal)
            {
                var animal2 = obj as Animal;
                result = String.Compare(animal2.Genus, this.Genus);
            }
            return result;
        }

        public static IComparer SortWeightAscending()
        {
            var comparer = new SortWeightAscendingHelper();
            return (IComparer)comparer;
        }
        public static IComparer SortGenusDescending()
        {
            throw new NotImplementedException();
        }

        class SortWeightAscendingHelper : IComparable
        {
            public int CompareTo(object obj)
            {
                throw new NotImplementedException();
            }
        }
        class SortGenusDescendingHelper : IComparable
        {
            public int CompareTo(object obj)
            {
                throw new NotImplementedException();
            }
        }

        // 2) declare properties and parameter constructor

        // 3) implement method ComareTo()
        // it comares Genus of type string - return result of method String.Compare
        // for this.genus and argument object
        // don't forget to cast object to Animal



        // 4) declare methods SortWeightAscending(), SortGenusDescending()
        // they are static and return IComparer
        // return type is custed from constructor of classes SortWeightAscendingHelper,
        // SortGenusDescendingHelper calling


        // 5) declare 2 nested private classes SortWeightAscendingHelper, SortGenusDescendingHelper
        // they implement interface IComparer
        // every nested class has implemented method Comare with 2 parameters of object and return int
        // class SortWeightAscendingHelper sort weight by ascending
        // class SortGenusDescendingHelper sort genus by descending

    }
}
