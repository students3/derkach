﻿using Lesson.Barbershop;
using Lesson.Barbershop.Beverage;
using Lesson.Barbershop.Tools;
using System;

namespace Lesson
{
    class Program
    {
        static void Main(string[] args)
        {
            // Man arrrived
            var man = new Man { Name = "Ivan" };
            var menHairCut = new MenHairCut(new Coffee());
            menHairCut.MakeHairCut(man, new Scissors(), new Comb());

            Console.WriteLine();

            // Woman arrrived
            var woman = new Woman { Name = "Tanya" };
            var womanHairCut = new WomenHairCut(new Tea());
            womanHairCut.MakeHairCut(woman, new Scissors(), new Comb());

            Console.ReadLine();
        }
    }
}
