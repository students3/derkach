﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Barbershop.Tools
{
    public interface ITool
    {
        string ToolName { get;}
    }
}
