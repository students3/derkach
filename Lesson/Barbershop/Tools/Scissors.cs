﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Barbershop.Tools
{
    public class Scissors: ICuttingTool
    {
        public string ToolName { get; }
        public Scissors()
        {
            this.ToolName = "Scissors";
        }
    }
}
