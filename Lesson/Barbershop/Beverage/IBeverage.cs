﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson.Barbershop.Beverage
{
    public interface IBeverage
    {
        string BevName { get; set; }
    }
}
