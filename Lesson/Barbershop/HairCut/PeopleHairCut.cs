﻿using Lesson.Barbershop.Beverage;
using Lesson.Barbershop.People;
using Lesson.Barbershop.Tools;
using System;

namespace Lesson.Barbershop
{
    public abstract class PeopleHairCut<T> where T : IPerson
    {
        private IBeverage beverage = new Coffee();

        public PeopleHairCut(IBeverage beverage)
        {
            this.beverage = beverage;
        }

        public void CleanRoom<V, U>(V tool1, U tool2) where V : ITool where U : ITool
        {
            Console.WriteLine("Cleaning room with: " + tool1.ToolName + " and " + tool2.ToolName);
        }

        public void MakeHairCut(T person)
        {
            this.OfferBeverageToClient(this.beverage);
            Console.WriteLine("Making haircut for: " + person.Name);
            this.SayAboutDiscount();
        }

        public void MakeHairCut<V>(T person, V tool1) where V : ICuttingTool
        {
            Console.WriteLine("Making haircut for: " + person.Name + " with tool: " + tool1.ToolName);
        }

        public void MakeHairCut<V, U>(T person, V tool1, U tool2) where V : ICuttingTool where U : ICuttingTool
        {
            this.OfferBeverageToClient(this.beverage);
            Console.WriteLine("Making haircut for: " + person.Name +
                              " with tool: " + tool1.ToolName + " and " + tool2.ToolName);
            this.SayAboutDiscount();
        }
        
        private void OfferBeverageToClient<Z>(Z beverage) where Z : IBeverage
        {
            Console.WriteLine($"Offering {beverage.BevName} to client");
        }

        private void SayAboutDiscount()
        {
            Console.WriteLine("Come to us next time and you'll get discount");
        }
    }


}
