﻿using Lesson.Barbershop.Beverage;

namespace Lesson.Barbershop
{
    public sealed class WomenHairCut : PeopleHairCut<Woman>
    {
        public WomenHairCut(IBeverage beverage) : base(beverage)
        {
        }
    }
}
