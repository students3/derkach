﻿using Lesson.Barbershop.Beverage;

namespace Lesson.Barbershop
{
    public sealed class MenHairCut : PeopleHairCut<Man>
    {
        public MenHairCut(IBeverage beverage) : base(beverage)
        {
        }
    }
}
