﻿using Lesson.Barbershop.People;
using Lesson.Barbershop.Tools;

namespace Lesson.Barbershop
{
    public interface IHairCut<T> where T : IPerson
    {
        void MakeHairCut(T person); // Comb, Scissors
        void MakeHairCut<V>(T person, V tool1) where V : ICuttingTool; // Lighter

        void MakeHairCut<V, U>(T person, V tool1, U tool2) where V : ICuttingTool 
                                                           where U : ICuttingTool; // Comb, Scissors

        void CleanRoom<V, U>(V tool1, U tool2) where V : ITool
                                               where U : ITool; // Bezom, Showel
    }
}
