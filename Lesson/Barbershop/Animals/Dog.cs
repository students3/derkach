﻿using Lesson.Barbershop.People;

namespace Lesson.Barbershop.Animals
{
    public class Dog : IAnimal
    {
        public string Name { get; set; }

        public PersonSex Sex { get; private set; }
    }
}
