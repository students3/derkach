﻿using Lesson.Barbershop.People;

namespace Lesson.Barbershop
{
    public class Man : IPerson
    {
        public string Name { get; set; }
        public PersonSex Sex { get; private set; }

        public Man()
        {
            this.Sex = PersonSex.Male;
        }
    }
}
