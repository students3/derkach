﻿namespace Lesson.Barbershop.People
{
    public interface IPerson
    {
        string Name { get; set; }
        PersonSex Sex { get; }
    }
}
