﻿using Lesson.Barbershop.People;

namespace Lesson.Barbershop
{
    public class Woman : IPerson
    {
        public string Name { get ; set; }
        public PersonSex Sex { get; private set; }

        public Woman()
        {
            this.Sex = PersonSex.Female;
        }
    }
}
