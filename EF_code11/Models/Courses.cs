﻿using EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_code11.Models
{
    [Table("Courses")]
   public class Courses
    {
        [Key]
        [Column(Order =1)]
        public int CourseId { get; set; }

        [Key]
        public int LecturerId { get; set; }



        public string CourseName { get; set; }

        public virtual Lecturer Lecturer { get; set; } // navigation property

    }
}
