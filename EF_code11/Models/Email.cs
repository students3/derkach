﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.CodeFirst.Models
{
    [Table("Emails")]
    public class Email
    {
        [Key]
        public int Id { get; set; }     // key for first field
        public string EmailValue { get; set; }
        public int LecturerId { get; set; }

        // even email have 1 field for foreign key from lecturer,
        //virtual allow for it to be overridden in a derived class
        public virtual Lecturer Lecturer { get; set; } // navigation property
    }

}

