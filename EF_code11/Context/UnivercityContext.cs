﻿using EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_code11.Context
{
    public class UnivercityContext: DbContext
    {
        public UnivercityContext() : base("UnivercityEntities")
        {

        }

        public DbSet<Email> Emails { get; set; } // email table
        public DbSet<Lecturer> Lecturers { get; set; } // Lecturer table
    }
}
