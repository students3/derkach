namespace EF_code11.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailValue = c.String(),
                        LecturerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lecturers", t => t.LecturerId, cascadeDelete: true)
                .Index(t => t.LecturerId);
            
            CreateTable(
                "dbo.Lecturers",
                c => new
                    {
                        LecturerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        Age = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LecturerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Emails", "LecturerId", "dbo.Lecturers");
            DropIndex("dbo.Emails", new[] { "LecturerId" });
            DropTable("dbo.Lecturers");
            DropTable("dbo.Emails");
        }
    }
}
