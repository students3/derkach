﻿using Lesson21.Attributes;

namespace Lesson21.Enum
{
    public enum ProjectStatus
    {
        [StringValue("Traded")]  // dropdown value
        TR,                      // db stored value

        [StringValue("Offered")]
        OF,

        [StringValue("Issued")]
        IS

    }
}
