﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson22
{
    public class Calculator
    {
        public int Multiply(int x, int y)
        {
            return x * y;
        }

        public int Sum(int x, int y)
        {
            return x + y;
        }
        public int Del(int x, int y)
        {
            return x / y;
        }



        //Convert into LINQ !!!
        public int SumArray(int[] input)
        {
            return input.Sum();
        }
    }
}
