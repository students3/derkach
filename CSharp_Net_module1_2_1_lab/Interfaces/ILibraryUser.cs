﻿
namespace CSharp_Net_module1_2_1_lab.Interfaces
{
    public interface ILibraryUser
    {
        void AddBook();
        void RemoveBook();
        void BookInfo();
        void BooksCount();
    }
}
