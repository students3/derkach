﻿using CSharp_Net_module1_2_1_lab.Classes;
using System;
using System.Collections.Generic;
using static CSharp_Net_module1_2_1_lab.Classes.LibraryUser;

namespace CSharp_Net_module1_2_1_lab
{

    class Program
    {
        static void Main(string[] args)
        {
            // 8) declare 2 objects. Use default and paremeter constructors
            LibraryUser user1 = new LibraryUser(),
            user2 = new LibraryUser("Maria", "Ivanenko", "+380447777777", 2);
            Console.WriteLine("User1 " + user1.FirstName + " " + user1.LastName);
            Console.WriteLine("User2 " + user2.FirstName + " " + user2.LastName + "\n");

            //// 9) do operations with books for all users: run all methods for both objects
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Adding a New Book\n");
            Console.ResetColor();
            LibraryUser libraryUser = new LibraryUser();
            Printer handler1 = new Printer();
            libraryUser.OnBookAdded += handler1.PrintMessage;
            libraryUser.AddBook();

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\n\nThe Current Count of Books\n");
            Console.ResetColor();
            libraryUser.BooksCount();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\nRemoving a Book\n");
            Console.ResetColor();
            libraryUser.OnBookRemoved += handler1.PrintMessage;
            libraryUser.RemoveBook();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\n\nSearching the Book\n");
            Console.ResetColor();
            libraryUser.BookInfo();

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\n\nThe Current Count of Books\n");
            Console.ResetColor();
            libraryUser.BooksCount();
            Console.ReadLine();
        }
    }
}
