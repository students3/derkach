﻿using CSharp_Net_module1_2_1_lab.Interfaces;
using System;
using System.Collections.Generic;

namespace CSharp_Net_module1_2_1_lab.Classes
{
    public class LibraryUser : ILibraryUser
    {
        public string FirstName { get;}
        public string LastName { get;}
        public int ID { get; }
        public string Phone { get; set; }
        public int BookLimit { get; }
        List<string> bookList = new List<string>() { "The Art of War", "Pride and Prejudice", "\nThe Prince", "\nThe Odyssey" };
        //private string[] bookList = { "", "", "", "" }; // array
        //public string this[int BookList] // indexator of array
        //{
        //    set
        //    {
        //        bookList[BookList] = value;
        //    }

        //    get
        //    {
        //        return bookList[BookList];
        //    }
        //}

        public LibraryUser()  //default constructor
        {
            FirstName = "Anna";
            LastName = "Andreeva";
            Phone = "652565325";
            ID = 10;
        }

        public LibraryUser(string firstName, string lastName, string phone, int id) // parameter constructor
        {
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            ID = id;
        }
        public delegate void WorkingWithBookHandler(string message);
        public event WorkingWithBookHandler OnBookAdded;
        public event WorkingWithBookHandler OnBookRemoved;

        //methods

        public void AddBook()
            {
                Console.WriteLine("User 1: add Harry Potter");
                string a = "\nHarry Potter";
                bookList.Add(a);
                Console.WriteLine("User 2: add Sherlock Holmes");
                string b = "\nSherlock Holmes";
                bookList.Add(b);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nThe List of Books After Adding New Books:\n");
                Console.ResetColor();
                foreach (var index in bookList)
                {
                    Console.Write(index + " ");
                }
                if (OnBookAdded != null)
                OnBookAdded($"\n\nEvent: New Books were added: {a + " and " + b}");
                }

        public class Printer//The class, that react on adding or removing events
        {
            public void PrintMessage(string message)
            {
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(message);
                    Console.ResetColor();
                }
            }
        }

        public void BookInfo()
        {
            Console.WriteLine("User 1: search Art of War");
            Console.WriteLine("User 2: search Pride and Prejudice");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nThe Found Books:\n");
            Console.ResetColor();
            string book = bookList.Find((x) => x == "The Art of War");
            Console.WriteLine(book);
            string book2 = bookList.Find((x) => x == "Pride and Prejudice");
            Console.WriteLine(book2);
        }

        public void BooksCount()
        {
            Console.WriteLine(bookList.Count);
        }

        public void RemoveBook()
        {
            Console.WriteLine("User 1: remove Harry Potter");
            string a = "\nHarry Potter";
            bookList.Remove(a);
            Console.WriteLine("User 2: remove Sherlock Holmes");
            string b = "\nSherlock Holmes";
            bookList.Remove(b);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nThe List of Books After Removing Books:\n");
            Console.ResetColor();
            foreach (var index in bookList)
            {
                Console.Write(index + " ");
            }
            if (OnBookRemoved != null)
                OnBookRemoved($"\n\nEvent: The {a + " and " + b} Books were removed");
        }
    }


    }




