﻿using System;
using Business.Interfaces;

namespace Business.Products
{
    public class Mouse: IProducts
    {
        public string ProductName { get; set; }

        public string Manufacturer { get; set; }

        public DateTime DateCreated { get; set; }

        public Double Price { get; set; }

        public DateTime? DateSold { get; set; }

        public int Qty { get; set; }

        public bool MouseWire { get; set; }

        public Double BagSize { get; set; }

        public string FlashSize { get; set; }

        public string HDColor { get; set; }

        public Double DisplaySize { get; set; }

    }
}
