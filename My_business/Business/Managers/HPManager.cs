﻿using System;
using Business.Interfaces;

namespace Business.Managers
{
    public class HPManager: IManagers
    {
        public void SellProduct(IProducts product, int qty)
        {
            product.DateSold = DateTime.Now;
            product.Qty = qty;
        }

    }
}
