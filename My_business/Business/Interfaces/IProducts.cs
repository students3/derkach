﻿using System;

namespace Business.Interfaces
{
    public interface IProducts
    {
        string ProductName { get; set; }

        string Manufacturer { get; set; }

        DateTime DateCreated { get; set; }

        Double Price { get; set; }

        DateTime? DateSold { get; set; }

        int Qty { get; set; }

        Double BagSize { get; set; }

        string FlashSize { get; set; }

        string HDColor { get; set; }

        Double DisplaySize { get; set; }

        bool MouseWire { get; set; }
    }
}
