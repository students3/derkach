﻿
namespace Business.Enum
{
    public enum SortingOrderEnum
    {
        ProductName,
        Price,
        Qty
    }
}
