﻿
namespace Business.Enum
{
    public enum SellQtyEnum
    {
        Single = 1,
        InBatch3 = 3,
        InBatch5 = 5
    }
}
