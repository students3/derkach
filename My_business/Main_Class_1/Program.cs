﻿using System;
using Business.Managers;
using Business.Products;
using Business.Interfaces;
using Business.Enum;
using System.Linq;

namespace Main_Class_1
{
    class Program
    {
        static void Main(string[] args)
        {
            IProducts[] products = GenerateProducts();

            SellProduct(products);
            PrintProducts(products);
            PrintProductsNew(products);
            //SortProducts(OrderBy);
            Console.ReadLine();

        }
        static IProducts[] GenerateProducts()
        {
            IProducts bag = new Bag

            {
                ProductName = "Bag",
                Manufacturer = "Crown",
                Price = 600,
                DateCreated = new DateTime(2017, 8, 1),
                BagSize = 15.7
            };
            IProducts flash = new FlashDrive

            {
                ProductName = "Flash",
                Manufacturer = "A4Tech",
                Price = 100,
                DateCreated = new DateTime(2017, 8, 1),
                FlashSize = "8Gb"
            };
            IProducts headphones = new HeadPhones

            {
                ProductName = "HeadPhones",
                Manufacturer = "Samsung",
                Price = 500,
                DateCreated = new DateTime(2017, 8, 1),
                HDColor = "green"
            };
            IProducts laptop = new Laptop

            {
                ProductName = "Mac",
                Manufacturer = "Apple",
                Price = 50000,
                DateCreated = new DateTime(2017, 8, 1),
                DisplaySize = 15.7
            };
            IProducts mouse = new Mouse

            {
                ProductName = "Mouse",
                Manufacturer = "Logitech",
                Price = 300,
                DateCreated = new DateTime(2017, 8, 1),
                MouseWire = true
            };

            return new IProducts[] { bag, flash, headphones, laptop, mouse };
        }

        static void SellProduct(IProducts[] products)
        {

            for (int i = 0; i < products.Length; i++)
            {
                var item = products[i];

                if (item is Bag)
                {
                    IManagers manager = new BagManager();
                    manager.SellProduct(item, 15);
                }
                else if (item is FlashDrive)
                {
                    IManagers manager = new FDManager();
                    manager.SellProduct(item, 40);
                }
                else if (item is HeadPhones)
                {
                    IManagers manager = new HPManager();
                    manager.SellProduct(item, 10);
                }
                else if (item is Laptop)
                {
                    IManagers manager = new LManager();
                    manager.SellProduct(item, 15);
                }
                else if (item is Mouse)
                {
                    IManagers manager = new MouseManager();
                    manager.SellProduct(item, 30);
                }
            }
        }
        static void PrintProducts(IProducts[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                var products1 = products[i];

                string message = "Product: " + products1.ProductName + "\n" +
                                 "Manufacturer: " + products1.Manufacturer + "\n" +
                                 "Price: " + products1.Price + "\n" +
                                 "sold on: " + products1.DateSold?.ToString("yyyy MM") + "\n" +
                                 "in qty:  " + products1.Qty + "\n-------------";
                Console.WriteLine(message);
            }
        }
        static IProducts[] SortProducts(IProducts[] product, SortingOrderEnum orderBy) // enum for sorting   
        {
            //1) switch by enum for sorting
            IProducts[] result = product;

            switch (orderBy)
            {
                case SortingOrderEnum.ProductName:
                    result = product.OrderBy(x => x.ProductName).ToArray();
                    break;

                case SortingOrderEnum.Price:
                    result = product.OrderBy(x => x.Price).ToArray();
                    break;

                case SortingOrderEnum.Qty:
                    break;

                default:
                    break;
            }
                        
            return result;

        }
        static void PrintProductsNew(IProducts[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                var products1 = products[i];

                if (products1 is Bag)
                {
                    string message = "BagSize: " + products1.BagSize + "\n-------------";
                    Console.WriteLine(message);
                }
                else if (products1 is FlashDrive)
                {
                    string message = "FlashSize: " + products1.FlashSize + "\n-------------";
                    Console.WriteLine(message);
                }
                else if (products1 is HeadPhones)
                {
                    string message = "HDColor: " + products1.HDColor + "\n-------------";
                    Console.WriteLine(message);
                }
                else if (products1 is Laptop)
                {
                    string message = "DisplaySize_Laptop: " + products1.DisplaySize+ "\n-------------";
                    Console.WriteLine(message);
                }
                else if (products1 is Mouse)
                {
                    string message = "Wired mouse: " + products1.MouseWire + "\n-------------";
                    Console.WriteLine(message);
                }
            }
        }

    }
}
