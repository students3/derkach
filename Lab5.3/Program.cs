﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab5._3
{
    class Program
    {
        static void Main(string[] args)
        {
            TestSqlReader();
            Console.ReadLine();
        }
        private static void TestSqlReader()
        {
            string connstring = @"Data Source=SASHA-ПК\SQLEXPRESS;Encrypt=False;Integrated Security=True;User ID=SASHA-ПК\SQLEXPRESS;Initial catalog=Store;";
            // Data Source=SASHA-ПК\SQLEXPRESS;Initial Catalog=Store;Integrated Security=True
            string queryString = "Select * from dbo.Customers";

            using (var dbconn = new SqlConnection(connstring))
            {
                SqlCommand command = new SqlCommand(queryString, dbconn);
                dbconn.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(string.Format("{0} {1} {2} {3}", reader[0], reader[1], reader[2], reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }

        }
    }
}
