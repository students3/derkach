﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lesson24
{
    public class Program
    {
        static void Main(string[] args)
        {
            //OnlyThreadMeth();
            FirstThread();
        }

        public static void OnlyThreadMeth()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("Counter: {0}. thread: {1}", i,
                    Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(10);
            }
        }

        public static void FirstThread()
        {
            ThreadStart starter = new ThreadStart(OnlyThreadMeth);
            Thread thr_1 = new Thread(starter);
            Thread thr_2 = new Thread(starter);
            thr_1.Start();
            thr_2.Start();
            thr_1.Join();
            thr_2.Join();
            Console.Read();
        }
    }
}
